
# Kotlin Recipe App

## Key Features

- **Kotlin & MVVM Architecture:** Utilizes Kotlin with the MVVM pattern for scalable and maintainable code.
- **Jetpack Compose Navigation:** Employs a single activity and zero fragments approach for UI navigation.
- **REST API Integration:** Fetches recipe data from a network source to provide a wide range of recipes.
- **Database Caching:** Implements local database caching for offline access and enhanced performance.
- **Network Connectivity Monitoring:** Monitors connectivity status to manage data fetching policies.
- **Use Cases:** Encapsulates business logic in use cases to keep the code clean and manageable.
- **Datastore:** Uses Datastore (replacement for Shared Preferences) for lightweight data storage.
- **Unit Testing:** Incorporates unit tests to ensure code reliability and functionality.
- **MockWebServer:** Utilizes MockWebServer for testing network requests without hitting the actual API.
- **Kotlin Flow:** Leverages Kotlin Flow for handling asynchronous data streams within the application.

## Getting Started

### Prerequisites

- Android Studio Arctic Fox | 2020.3.1 or newer
- Kotlin plugin (should come with Android Studio)
- An Android device or emulator with Internet connectivity

